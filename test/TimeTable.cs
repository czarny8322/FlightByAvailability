﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace test
{
    public class TimeTable
    {
        public string Carrier { get; set; }      
        public string Boarding { get; set; }       
        public string Boarding_Time { get; set; }
        public string Arrival { get; set; }
        public string Arrival_Time { get; set; }
        public string FLYING_TIME { get; set; }
    }
}
