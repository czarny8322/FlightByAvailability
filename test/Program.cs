﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace test
{
    class Program
    {
        public object UIHelper { get; private set; }

        static void Main(string[] args)
        {

            //var date = GetData("A1AUGWAWPHX/UA9424/UA8832/UA917");            
            //DateTime myDate = DateTime.ParseExact(date, new string[] { "dMMM", "ddMMM" }, CultureInfo.GetCultureInfo("en-US"), DateTimeStyles.None);
            //var parseDateString = myDate.ToString("ddMMM", CultureInfo.GetCultureInfo("en-US")).ToUpper();



            ///var s = "19SEP";
            //CultureInfo arSA = new CultureInfo("en-US");
            //DateTime dt = DateTime.ParseExact(s, "ddMMM", CultureInfo.InvariantCulture);
            //var xx = dt.AddDays(1);
            //var tt = myDate.ToString("ddMMM", CultureInfo.GetCultureInfo("en-US")).ToUpper();

            //string str = "A19SEPWAWJFK/LO371/LH5826/UA9716";
            //var xx = getDate(str);
            //var flightNumber = getFlightNumbers(str);
            //bool _checkCommand = checkCommand(str);

            //var tt = new List<string>();
            //tt.Add(" LO  381  WEDNESDAY    19 SEP 18");
            //tt.Add("---------------------------------------------------------------");
            //tt.Add(" BRD TIME    T D/I  OFF TIME    T D/I   FLY/GROUND      EQP   E");
            //tt.Add(" WAW 0745       I   FRA 0945    1  I    2:00            738   E");
            //tt.Add("---------------------------------------------------------------");
            //tt.Add("TOTAL FLYING TIME  WAW - FRA      2:00");
            //tt.Add("---------------------------------------------------------------");
            //tt.Add("CLASSES");
            //tt.Add("WAW-FRA C  D  Z  F  P  A  R  Y  B  M  E  H  K  Q  G  T  S  V  ");

            //var index = 0;
            //var ttInfoSTR = string.Empty;
            //foreach (var i in tt)
            //{                
            //    if(i.Contains("BRD TIME"))
            //    {                    
            //        ttInfoSTR = tt[index+1];

            //    }
            //    index++;
            //}

            //string _boarding = ttInfoSTR.Substring(0, 4).Trim();
            //string _boarding_Time = ttInfoSTR.Substring(5, 4).Trim();
            //string _arrival = ttInfoSTR.Substring(20, 3).Trim();
            //string _arrival_Time = ttInfoSTR.Substring(24, 4).Trim();

            //test

            //var date = GetData("A01AUGWAWPHX/UA9424/UA8832/UA917");
           // var org = GetOrgin("A01AUGWAWPHX/UA9424/UA8832/UA917");
            //var dest = GetDestination("A01AUGWAWPHX/UA9424/UA8832/UA917");
        }

        private static string GetData(string terminalEntry)
        {
            string parseDateString = "";
            string availability = terminalEntry.Split('/')[0];
            var date = "";
            if (availability.StartsWith("A"))
            {
                availability = availability.Substring(1, availability.Length - 1);
                var orgDest = availability.Substring(availability.Length - 6);
                date = availability.Replace(orgDest, "");

                try
                {
                    DateTime myDate = DateTime.ParseExact(date, new string[] { "dMMM", "ddMMM" }, CultureInfo.GetCultureInfo("en-US"), DateTimeStyles.None);
                    parseDateString = myDate.ToString("ddMMM", CultureInfo.GetCultureInfo("en-US")).ToUpper();
                }
                catch (Exception)
                {
                    //UIHelper.Instance.ShowMessageBox("wrong availability date...", MessageBoxButton.OK, MessageBoxImage.Warning);
                    throw;
                }

            }

            return parseDateString;
        }


        private static string GetOrgin(string terminalEntry)
        {
            string availability = terminalEntry.Split('/')[0];
            string orgin = "";

            if (availability.StartsWith("A"))
            {
                availability = availability.Substring(1, availability.Length - 1);
                orgin = availability.Substring(availability.Length - 6).Trim();
            }
            return orgin.Substring(0, 3);
        }
        private static string GetDestination(string terminalEntry)
        {
            string availability = terminalEntry.Split('/')[0];
            string destination = "";

            if (availability.StartsWith("A"))
            {
                availability = availability.Substring(1, availability.Length - 1);
                destination = availability.Substring(availability.Length - 6).Trim();
            }
            return destination.Substring(3, 3);
        }

        private static bool checkCommand(string str)
        {
            if (str.StartsWith("A", StringComparison.OrdinalIgnoreCase))
            {
                if (str.Contains('/')){
                    var strAfterSplit = str.Split('/');
                    var firstCarrier = strAfterSplit[1];

                    if (Regex.IsMatch(firstCarrier, "^[A-Z]{2}[0-9]{1,4}$"))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        private static object getFlightNumbers(string str)
        {
            return str.Split('/').Skip(1).ToArray();
        }

        //private static object getDate(string str)
        //{
        //    return str.Substring(1, 5);
        //}
    }
}
