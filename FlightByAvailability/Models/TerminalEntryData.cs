﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Plugin.cs" company="Name of the company">
//  Copyright (c). All rights reserved.
// </copyright>
// <summary>
//   Plugin for Travelport smartpoint
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows;
using Travelport.Smartpoint.Helpers.UI;

namespace FlightByAvailability.Models
{
    internal class TerminalEntryData
    {
        private string _trvelDate;

        #region Properties

        public string TerminalEntry { get; set; }

        public string TrvelDate
        {
            get => string.IsNullOrEmpty(_trvelDate) ? GetData(TerminalEntry) : _trvelDate;
            set => _trvelDate = value;
        }

        public string Origin => GetOrgin(TerminalEntry);

        public string Destination => GetDestination(TerminalEntry);

        public IEnumerable<string> FlightNumbers => TerminalEntry.Split('/').Skip(1);

        public List<TimeTable> TimeTableInformations { get; set; }

        #endregion
        public TerminalEntryData(string terminalEntry)
        {
            TerminalEntry = terminalEntry;
            TimeTableInformations = new List<TimeTable>();
            GetTimeTableInformations();
        }

        private string GetData(string terminalEntry)
        {
            string parseDateString = "";
            string availability = terminalEntry.Split('/')[0];
            var date = "";
            if (availability.StartsWith("A"))
            {
                availability = availability.Substring(1, availability.Length - 1);
                var orgDest = availability.Substring(availability.Length - 6);
                date = availability.Replace(orgDest, "");

                try
                {
                    DateTime myDate = DateTime.ParseExact(date, new string[] { "dMMM", "ddMMM" }, CultureInfo.GetCultureInfo("en-US"), DateTimeStyles.None);
                    parseDateString = myDate.ToString("ddMMM", CultureInfo.GetCultureInfo("en-US")).ToUpper();
                }
                catch (Exception)
                {
                    UIHelper.Instance.ShowMessageBox("wrong availability date...", MessageBoxButton.OK, MessageBoxImage.Warning);
                    throw;
                }
            }

            return parseDateString;
        }
        private string GetOrgin(string terminalEntry)
        {
            string availability = terminalEntry.Split('/')[0];
            string orgin = "";

            if (availability.StartsWith("A"))
            {
                availability = availability.Substring(1, availability.Length - 1);
                orgin = availability.Substring(availability.Length - 6).Trim();
            }
                return orgin.Substring(0, 3);
        }
        private string GetDestination(string terminalEntry)
        {
            string availability = terminalEntry.Split('/')[0];
            string destination = "";

            if (availability.StartsWith("A"))
            {
                availability = availability.Substring(1, availability.Length - 1);
                destination = availability.Substring(availability.Length - 6).Trim();
            }
            return destination.Substring(3, 3);
        }
        

        private void GetTimeTableInformations()
        {

            foreach (var flight in FlightNumbers)
            {
                var response = UIHelper.Instance.CurrentTEControl.Connection.SendEntry($"TT{flight}/{TrvelDate}");

                var isEmpty = response.Any(m => m.Contains("INVALID FLIGHT/DATE"));
                if (isEmpty)
                {
                    try
                    {
                        CultureInfo enUs = new CultureInfo("en-US");
                        DateTime dt = DateTime.ParseExact(TrvelDate, "ddMMM", CultureInfo.InvariantCulture);
                        var nextDay = dt.AddDays(1).ToString("ddMMM", enUs).ToUpper();
                        TrvelDate = nextDay;
                        response = UIHelper.Instance.CurrentTEControl.Connection.SendEntry($"TT{flight}/{TrvelDate}");

                        if (response.Any(m => m.Contains("INVALID FLIGHT/DATE")))
                        {
                            UIHelper.Instance.ShowMessageBox("No more results to show", MessageBoxButton.OK, MessageBoxImage.Information);
                            return;
                        }
                    }
                    catch (Exception ex)
                    {
                        UIHelper.Instance.ShowMessageBox($"Search by availability error: {ex.Message}");
                        return;
                    }
                }

                var carrier = flight.Substring(0, 2);

                var index = 0;
                var ttInfoStr = string.Empty;
                string boarding = string.Empty;
                string boardingTime = string.Empty;
                string arrival = string.Empty;
                string arrivalTime = string.Empty;
                string flightNumber = string.Empty;


                foreach (var row in response)
                {
                    if (row.Contains("BRD TIME"))
                    {
                        bool endLine = true;
                        int lastLineIndex = 0;
                        while (endLine)
                        {
                            var line = response[index];                            
                            if (line.Contains("----"))
                            {
                                lastLineIndex = index - 1;
                                endLine = false;
                            }
                            index++;
                        }
                        ttInfoStr = response[lastLineIndex];
                    }
                    index++;
                }

                if (!string.IsNullOrEmpty(ttInfoStr))
                {
                    boarding = ttInfoStr.Substring(0, 4).Trim();
                    boardingTime = ttInfoStr.Substring(5, 4).Trim();
                    arrival = ttInfoStr.Substring(20, 3).Trim();
                    arrivalTime = ttInfoStr.Substring(24, 4).Trim();
                    flightNumber = flight.Substring(2).Trim();
                }

                TimeTableInformations.Add(new TimeTable
                {
                    Carrier = carrier,
                    Boarding = boarding,
                    Boarding_Time = boardingTime,
                    Arrival = arrival,
                    Arrival_Time = arrivalTime,
                    FlightNumber = flightNumber
                });
            }
        }
    }
}
