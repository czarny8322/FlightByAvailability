﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TelemetrySdk.Helpers;

namespace FlightByAvailability.Helpers
{
    static class TeleHelper
    {       
        public static string telemetryClientId = "FlightByAvailability";

        public static void LogCustomObject(object obj)
        {
            TelemetryHelper.Instance.LogCustomObject(telemetryClientId, obj);
        }
        public static void LogException(string exception)
        {
            TelemetryHelper.Instance.LogException(telemetryClientId, exception);
        } 

    }
}
