﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Plugin.cs" company="Name of the company">
//  Copyright (c). All rights reserved.
// </copyright>
// <summary>
//   Plugin for Travelport smartpoint
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Windows;
using FlightByAvailability.Helpers;
using FlightByAvailability.Models;
using TelemetrySdk.Helpers;
using TelemetrySdk.Models;
using Travelport.Smartpoint.Collections;
using Travelport.Smartpoint.Common;
using Travelport.Smartpoint.Helpers.Core;
using Travelport.Smartpoint.Helpers.UI;
using Travelport.Smartpoint.Plugins.AirAvailability;
using Travelport.Smartpoint.Plugins.AirAvailability.ViewModels;
using Travelport.Smartpoint.Plugins.AirAvailability.Views;

namespace FlightByAvailability
{
    /// <summary>
    /// Plugin for extending Smartpoint application
    /// </summary>
    [SmartPointPlugin(Developer = "Adam Stawarz",
                      Company = "Travelport",
                      Description = "Flight by availability Plugin",
                      Version = "0.2",
                      Build = "Build version of Smartpoint application")]
    public class Plugin : HostPlugin
    {
        /// <summary>
        /// Executes the load actions for the plugin.
        /// </summary>
        public override void Execute()
        {
            // Attach a handler to execute when the Smartpoint application is ready and all windows are loaded.
            CoreHelper.Instance.OnSmartpointReady += OnSmartpointReady;

        }

        #region Handlers

        /// <summary>
        /// Handles the Smartpoint Ready event of the Instance control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="Travelport.Smartpoint.Helpers.Core.CoreHelperEventArgs"/> instance containing the event data.</param>
        private void OnSmartpointReady(object sender, CoreHelperEventArgs e)
        {
            CoreHelper.Instance.TerminalCommunication.OnTerminalPreSend += OnTerminalPreSend;

            var logAction = new Action<Exception>((ex) =>
            {
                //use your logger here:)
                UIHelper.Instance.ShowMessageBox("Telemetry API error: " + ex.Message, MessageBoxButton.OK, MessageBoxImage.Warning);
                //ex.Dump();
            });

            var getCurrentPccFunc = new Func<string>(() =>
            {
                // put here some method that can take current PCC
                return UIHelper.Instance.CurrentTEControl.Connection.CommunicationFactory.GetCurrentPcc();
            });
            

            //change this product ID
            //var telemetryClientId = "FlightByAvailability";

            //product version can be taken from executing  assembly like below:
            var assemblyPath = new Uri(Assembly.GetExecutingAssembly().CodeBase).LocalPath;
            var version = System.Reflection.AssemblyName.GetAssemblyName(assemblyPath).Version;

            //initialise telemetry 
            // 1) send To API interval [sec], 2) Max batch logs size (optional - 30 default), 5) action log (optional/null default)
            TelemetryHelper.Instance.RegisterProduct(TeleHelper.telemetryClientId,
                new ClientMetadata
                {
                    ProductId = 10, // request for productId
                    LogAction = logAction, // optional
                    ProcustVersion = version.ToString() // or "1.9.9.1"
                }
            );
            TelemetryHelper.Instance.SetCurrentPcc = getCurrentPccFunc;            
            TelemetryHelper.Instance.LogLaunchEvent(TeleHelper.telemetryClientId);

            
            //TelemetryHelper.Instance.LogCustomObject(telemetryClientId, new { Avail_Data = new List<String> { "test", "test1" } }, "runPluginData");
            //TelemetryHelper.Instance.LogException(telemetryClientId, $"Exception message {i}...");
        }

        private void OnTerminalPreSend(object sender, TerminalEventArgs e)
        {            
            var terminalEntry = e.TerminalEntry;
            if (string.IsNullOrEmpty(terminalEntry))
                return;

            //var currentPcc = e.Connection.HostUserSettings.CurrentPcc;

            if (!terminalEntry.StartsWith("BB", StringComparison.OrdinalIgnoreCase))
                return;
            e.Handled = true;

            try
            {
                terminalEntry = terminalEntry.Replace("BB", "");

                if (ValidateCommand(terminalEntry))
                {                    
                    RunFlightByAvailability(terminalEntry);
                }
                else
                {
                    UIHelper.Instance.ShowMessageBox("Command is nod valid, please try again...\n Example command: BBA1AUGWAWPHX/UA9424/UA8832/UA917", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
                UIHelper.Instance.ClearTextAfterActiveSOM(e.TEControl);
            }
            catch (Exception ex)
            {
                //dopisac metode
                TelemetryHelper.Instance.LogException(TeleHelper.telemetryClientId, ex.ToString());
                throw;
            }
            //e.Handled = true;
        }

        private void RunFlightByAvailability(string terminalEntry)
        {
            var terminalEntryData = new TerminalEntryData(terminalEntry);

            TeleHelper.LogCustomObject(terminalEntryData);

            if (terminalEntryData.TimeTableInformations.Count != 0)
            {
                string searchCommand = CreateSearchCommand(terminalEntryData);
                CoreHelper.Instance.SendHostCommand(searchCommand, UIHelper.Instance.CurrentTEControl, true, true);

                var terminal = UIHelper.Instance.CurrentTEControl;
                var view = terminal.TerminalRTB.Document.Blocks.FirstOrDefault(b => b is AvailabilityScreenView);
                {
                    if (view != null)
                    {
                        var vm = view.DataContext as AvailabilityScreenViewModel;
                        if (!vm.AvailLines.Any())
                        {
                            //UIHelper.Instance.ShowMessageBox("")
                            return;
                        }
                        ObservableRangeCollection<SpanAirSegmentViewModel> allDataForView;

                        int lastSegmentCount = 0;
                        var goups = GetAirAvailabilityGroups(vm.AvailLines);
                        var filterResults = TryFindFlights(goups, terminalEntryData.TimeTableInformations);
                        lastSegmentCount = vm.AvailLines.Count;
                        var lastFilterResults = filterResults;

                        int target = 0;
                        bool isMoreOptionsExist = true;

                        while (isMoreOptionsExist && target != 5 && filterResults.Count == 0)
                        {
                            vm.MoreCommand.Execute(null);
                            isMoreOptionsExist = vm.VisibleMoreLink;
                            target++;

                            goups = GetAirAvailabilityGroups(vm.AvailLines);
                            filterResults = TryFindFlights(goups, terminalEntryData.TimeTableInformations);
                            lastSegmentCount = vm.AvailLines.Count;
                            lastFilterResults = filterResults;
                        }

                        vm.AvailLines = new ObservableRangeCollection<SpanAirSegmentViewModel>(filterResults);

                        // copy of existing command
                        var standardMoreCommand = vm.MoreCommand;
                        //set up yours implementation
                        vm.MoreCommand = new Travelport.MvvmHelper.RelayCommand(obj =>
                        {
                            standardMoreCommand.Execute(null);

                            allDataForView = vm.AvailLines;
                            int itemsToTake = allDataForView.Count - lastSegmentCount;
                            lastSegmentCount = allDataForView.Count;

                            var takeLast15Items = allDataForView.Skip(Math.Max(0, allDataForView.Count - itemsToTake));
                            var availLines = new ObservableRangeCollection<SpanAirSegmentViewModel>(takeLast15Items);

                            goups = GetAirAvailabilityGroups(availLines);
                            filterResults = TryFindFlights(goups, terminalEntryData.TimeTableInformations);
                            if (filterResults.Count > 0)
                            {
                                lastFilterResults = filterResults;
                            }

                            vm.AvailLines = new ObservableRangeCollection<SpanAirSegmentViewModel>(filterResults);

                            if (filterResults.Count != 0) return;
                            UIHelper.Instance.ShowMessageBox("No more results to show", MessageBoxButton.OK, MessageBoxImage.Information);
                            vm.AvailLines = new ObservableRangeCollection<SpanAirSegmentViewModel>(lastFilterResults);
                        });
                        //Refresh view
                        vm.OnPropertyChanged("MoreCommand");

                        //}
                        //UIHelper.Instance.ShowMessageBox("all segments from -> to", vm.AvailLines.Select(a => "Flignt from: " + a.Segment.Origin.Code + " to " + a.Segment.Destination.Code).ToList(), MessageBoxImage.Information);
                    }
                }
            }

        }

        private List<SpanAirSegmentViewModel> TryFindFlights(List<List<SpanAirSegmentViewModel>> goups, List<TimeTable> timeTableInformations)
        {
            List<SpanAirSegmentViewModel> result = new List<SpanAirSegmentViewModel>();

            List<string> groupFlights;

            List<string> timeTableFlights = GetFlightNumbersList(timeTableInformations);


            foreach (var group in goups)
            {
                groupFlights = GetFlightNumbersList(group);

                if (groupFlights.Count >= timeTableFlights.Count)
                {
                    var isFind = ListHelper<string>.ContainsAllItems(groupFlights, timeTableFlights);
                    if (isFind)
                    {
                        result.AddRange(group);
                    }
                }
            }

            if (result.Count > 0)
            {
                return result;
            }
            List<SpanAirSegmentViewModel> emptylist = new List<SpanAirSegmentViewModel>();
            return emptylist;
        }

        private List<string> GetFlightNumbersList(List<TimeTable> timeTableInformations)
        {
            var timeTableFlights = new List<string>();

            foreach (var i in timeTableInformations)
            {
                string timeTableFlight = $"{i.Carrier.Trim()}{i.FlightNumber.Trim()}";
                timeTableFlights.Add(timeTableFlight);
            }
            return timeTableFlights;
        }

        private List<string> GetFlightNumbersList(List<SpanAirSegmentViewModel> group)
        {
            List<string> groupFlights = new List<string>();

            foreach (var segment in group)
            {
                string _groupFlights = $"{segment.Segment.Carrier.Code.Trim()}{segment.Segment.FlightNumber.ToString().Trim()}";
                groupFlights.Add(_groupFlights);
            }
            return groupFlights;
        }

        private List<List<SpanAirSegmentViewModel>> GetAirAvailabilityGroups(ObservableRangeCollection<SpanAirSegmentViewModel> availLines)
        {

            List<List<SpanAirSegmentViewModel>> totalGroup = new List<List<SpanAirSegmentViewModel>>();
            List<SpanAirSegmentViewModel> goup = new List<SpanAirSegmentViewModel>();

            foreach (var item in availLines)
            {

                if (item.ConnectingFromLine == null)
                {
                    if (goup.Count != 0)
                    {
                        totalGroup.Add(goup);
                        goup = new List<SpanAirSegmentViewModel>();
                    }
                }
                goup.Add(item);

                if (availLines.IndexOf(item) == availLines.Count - 1)
                {
                    totalGroup.Add(goup);
                }
            }
            return totalGroup;
        }

        private string CreateSearchCommand(TerminalEntryData terminalEntryData)
        {
            string date = terminalEntryData.TrvelDate;
            string destFrom = terminalEntryData.Origin;
            string destTo = terminalEntryData.Destination;
            List<TimeTable> timeTableInformations = terminalEntryData.TimeTableInformations;

            string modyficator = string.Empty;
            string firstFlightTime = terminalEntryData.TimeTableInformations[0].Boarding_Time;
            bool isMatch = false;

            if (timeTableInformations.Count == 1)
            {
                if (destTo == timeTableInformations[0].Arrival)
                {
                    modyficator += $".D/{timeTableInformations[0].Carrier}";
                }
                else
                {
                    modyficator += $".C1/{timeTableInformations[0].Carrier}.{timeTableInformations[0].Arrival}";
                }
            }
            else if (timeTableInformations.Count == 2)
            {
                for (int i = 0; i < timeTableInformations.Count; i++)
                {
                    var lastTransfer = timeTableInformations[timeTableInformations.Count - 1].Arrival;

                    if (i == 0)
                    {
                        if (destTo == lastTransfer)
                        {
                            modyficator += $".C1/{timeTableInformations[0].Carrier}.{timeTableInformations[0].Arrival}";
                            isMatch = true;
                        }
                        else
                        {
                            modyficator += $".C2/{timeTableInformations[0].Carrier}.{timeTableInformations[0].Arrival}";
                        }
                    }

                    if (i != 0)
                    {
                        if (i == timeTableInformations.Count - 1)
                        {
                            if (isMatch)
                            {
                                modyficator += $"/{timeTableInformations[i].Carrier}";
                            }
                            else
                            {
                                modyficator += $"/{timeTableInformations[i].Carrier}.{timeTableInformations[i].Arrival}";
                            }
                        }
                    }
                }
            }
            else //more than 3 options
            {
                for (int i = 0; i < timeTableInformations.Count; i++)
                {
                    var lastTransfer = timeTableInformations[timeTableInformations.Count - 1].Arrival;

                    if (i == 0)
                    {
                        if (destTo == lastTransfer)
                        {
                            modyficator += $".C2/{timeTableInformations[0].Carrier}.{timeTableInformations[0].Arrival}";
                            isMatch = true;
                        }
                        else
                        {
                            modyficator += $".C3/{timeTableInformations[0].Carrier}.{timeTableInformations[0].Arrival}";
                        }
                    }

                    if (i != 0)
                    {
                        if (i == timeTableInformations.Count - 1)
                        {
                            if (isMatch)
                            {
                                modyficator += $"/{timeTableInformations[i].Carrier}";
                            }
                            else
                            {
                                modyficator += $"/{timeTableInformations[i].Carrier}.{timeTableInformations[i].Arrival}";
                            }
                        }
                        else
                        {
                            modyficator += $"/{timeTableInformations[i].Carrier}.{timeTableInformations[i].Arrival}";
                        }
                    }

                }
            }


            var command = $"A{date}{destFrom}{destTo}{modyficator}";
            return command;
        }




        private static bool ValidateCommand(string terminalEntry)
        {
            return Regex.IsMatch(terminalEntry, @"^A\d{1,2}[A-Z]{9}\/(?:[A-Z]{1,2}\d{1,5}\/)+[A-Z]{1,2}\d{1,5}$|^A\d{1,2}[A-Z]{9}\/[A-Z]{1,2}\d{1,5}$", RegexOptions.IgnoreCase);
        }

        // var response = UIHelper.Instance.CurrentTEControl.Connection.SendEntry("+H");
        // UIHelper.Instance.ShowMessageBox("No active PNR found. Please open PNR and try again.", MessageBoxButton.OK, MessageBoxImage.Warning);
        //A19SEPWAWJFK/LO371/LH5826/UA9716
        //TTLO371/19SEP
        //A19SEPWAWJFK/LO.STR/LH.ZRH/UA.0800

        #endregion
    }
}
