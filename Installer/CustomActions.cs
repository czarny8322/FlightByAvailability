﻿using Microsoft.Deployment.WindowsInstaller;
using System;
using System.Threading;

namespace Installer
{
    public static class CustomActions
    {
        [CustomAction]
        public static ActionResult OnInstall(Session session)
        {
            //MessageBox.Show("Add", "Aaaaaaaaaaaaaa");
            try
            {
                DeletePluginCache();
                //var installLcation = session.Property("INSTALLDIR");
                return ActionResult.Success;
            }
            catch (Exception)
            {
                return ActionResult.Failure;
            }
        }

        public static void DeletePluginCache(string userRoamingDirectoryPath = null)
        {
            //userPluginsCacheFilePath != null ? new FileInfo(userPluginsCacheFilePath) :
            var cachedPluginsFile = new System.IO.FileInfo(System.IO.Path.Combine((userRoamingDirectoryPath ?? Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData)) + "\\Travelport\\Smartpoint", "CachedPlugins.xml"));
            if (cachedPluginsFile.Exists)
            {
                var count = 0;
                while (true)
                {
                    try
                    {
                        cachedPluginsFile.Delete();
                        break;
                    }
                    catch (System.IO.IOException)
                    {
                        Thread.Sleep(200);
                        count++;
                        if (count > 24)
                            break;
                    }
                }
            }
        }

    }


}