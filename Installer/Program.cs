﻿using System;
using System.Diagnostics;
using System.Linq;
using WixSharp;
using WixSharp.CommonTasks;
using WixSharp.Controls;
using WixSharp.Forms;

namespace Installer
{
    public class Program
    {
        public static void Main()
        {

            try
            {
                var project = new Project("Flight By Availability",
                    new Dir(@"%ProgramFiles%\Travelport\Smartpoint",
                        new File("FlightByAvailability.dll"),
                        new File("TelemetrySdk.dll"),
                        new File("TelemetrySdk.dll.config")),
                    new Property("ALLUSERS", "1"),
                    //new Property("LicenseAccepted", "1"),
                    //https://www.advancedinstaller.com/user-guide/msiexec.html
                    new ElevatedManagedAction(CustomActions.OnInstall, Return.check, When.After, Step.InstallFiles, Condition.NOT_Installed)
                    {
                        Execute = WixSharp.Execute.deferred,
                    },
                    new CloseApplication(new Id("Travelport.Smartpoint.App"), "Travelport.Smartpoint.App.exe", true, false)
                    {
                        Timeout = 15,
                    }
                )
                {
                    InstallPrivileges = InstallPrivileges.elevated,
                    //SourceBaseDir = @"~..\..\..\FlightByAvailability\bin\Debug\",
                    SourceBaseDir = @"C:\Projects\AvailabilitySearchByFlights\dev\FlightByAvailability\bin\Debug",
                    GUID = new Guid("1f94d4c2-30a9-4ac3-8361-ce0cd651055c")
                };
                
                project.MajorUpgradeStrategy = new MajorUpgradeStrategy
                {
                    UpgradeVersions = VersionRange.OlderThanThis,
                    PreventDowngradingVersions = VersionRange.NewerThanThis,
                    NewerProductInstalledErrorMessage = "Newer version already installed",
                    RemoveExistingProductAfter = Step.InstallFinalize
                };
                

                project.SetNetFxPrerequisite(Condition.Net462_Installed, ".NET Framework 4.6.2 is required to continue installation. Please install .NET Framework 4.6.2 and try again");
                
                var pluginDllPath = System.IO.Path.Combine(project.SourceBaseDir, @"FlightByAvailability.dll");
                var fullPath = System.IO.Path.GetFullPath(pluginDllPath);

                Console.WriteLine("Plugin main dll filePath: " + fullPath);
                project.Version = new Version(FileVersionInfo.GetVersionInfo(pluginDllPath).FileVersion);
                project.OutFileName = "FlightByAvailability_" + project.Version;
                project.WixSourceGenerated += xDoc =>
                {
                    var productXelement = xDoc.Root.Descendants().FirstOrDefault();
                    if (productXelement == null) return;
                    var manufacturer = productXelement.Attribute("Manufacturer");
                    if (manufacturer != null)
                        manufacturer.SetValue("Travelport");
                };               

                //project.LicenceFile = @"C:\Projects\AvailabilitySearchByFlights\dev\Installer\2.rtf";

                Compiler.PreserveTempFiles = true;
                project.UI = WUI.WixUI_Common;
                //project.RemoveDialogsBetween(NativeDialogs.WelcomeDlg, NativeDialogs.InstallDirDlg);


                project.BuildMsi();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }


}